#Zadanie

Cieľom tohto projektu bolo vytvoriť sieťový komunikátor, ktorý dokáže posielať správy zadanej dlžky a textu na vstupe. Komunikácia mala prebiehať medzi klientskym modulom a serverom. Dodatočná funkcionalita vyžadovala možnosť odosielať zadaný text po fragmentoch o zadanej dĺžke, vykonávať kontrolu prijatej správy a iné.  
