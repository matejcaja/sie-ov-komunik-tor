import java.net.*;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class UDPClient extends Thread{
	
	private JTextArea textArea;
	private JTextArea debug;
	private JPanel contentPane;
	boolean end = false;
	int a;
	int b;
	int counter;
	int jednotky;
	int fragments = 2;
	final int MAX = 1471;
	long startTime;
	static boolean finish = false;
	int random_number = 0;
	int pocitadloVzorka = 0;
	int pocitadloVzorka_good = 0;
	long totalSize;
	long delay;
	long delaySize = 0;


    public UDPClient(JTextArea textArea, JPanel contentPane, JTextArea debug){
    	this.textArea = textArea;
    	this.contentPane = contentPane;
    	this.debug = debug;
    }
    
    public void run(){
    	sendCustom();
    }
       
    public void sendCustom(){
    	startTime = 0;
    	startTime = System.nanoTime();
    	end = false;
    	a = 0;
    	
    	int port = GUI.getPort();
    	String ip2 = GUI.getIP();
    	String count = GUI.getCount();
    	int velkost = Integer.parseInt(GUI.getVelkost());
    	    	
    	while(!end){
    		try{
    			DatagramSocket clientSocket = new DatagramSocket();
    			    			
    			while(a != Integer.parseInt(count)){//posielanie vzoriek
    				if(!(b > 1)) //ak nastane prerusenie na nejakom mieste, tak zaruci ze sa nezacne posielat cela fragmentacia vzorky
    					b = 1;
    				
    				InetAddress IPAddress = InetAddress.getByName(ip2);
					
					byte[] receiveData = new byte[1471];
   
	
					if(velkost > MAX){
						fragments = (findNumberOfFragments(velkost)+1);
					}
					
    				
    				while(b != fragments){//posielanie fragmenov
    					
    					if(counter == 3){
    						JOptionPane.showMessageDialog(contentPane, "Stanica nedostupna!","Connection error!", JOptionPane.ERROR_MESSAGE);
    						
    						return;
    					}
    					
    					Random generator = new Random( System.currentTimeMillis() );
                       	random_number = generator.nextInt(100);
    					
    					int suma = doFragmentation(b, velkost); //sfragmentuj vzorku a zadaj aka bude jej velkost    	
    					

    					if(suma < 18)//ak je mensia dlzka nastavi sa posielana dlzka na minimum ktore musi ist po sieti
    						suma = 18;
    					
    					byte[] sendData = new byte[suma];
    					    					
    					Arrays.fill(sendData, (byte)0);
    					Arrays.fill(receiveData, (byte)0);
    					
    					int i = 1;
    					for(i = 1; i < suma; i++){
    						sendData[i] = 1;
       					}    
    					
    					sendData[0] = packetHeader((i-1));

    					if(((Integer.parseInt(count)-a) == 1) && ((fragments - b) == 1)){
    						sendData[0] = setFin(sendData[0]);
    					}
    					
    					//if(counter != 3){
    						delay = System.nanoTime();
    						totalSize += (46+suma);//1 bajt je moja hlavicka
    						textArea.append("Posielam vzorku c."+(a+1)+" fragment c."+b+"\n"); 
    						vypisVyratanehoKodu(checkParita(sendData[0], 4));
    					
    						textArea.append("\n");
    					
    						delaySize += (System.nanoTime()-delay);
    						
    						
    					//}
    					
    					debug.append(random_number+"\n");
    					if(random_number < GUI.getDamage()){//poskodzujem paritu
    						sendData[b] = 0;
    					}
    					
    					DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);//3333

    					
    					/*****************************/
    					clientSocket.send(sendPacket);
    					counter++;
 
    				
    					DatagramPacket receivePacket = new DatagramPacket(new byte[18], 18);
       
    					clientSocket.setSoTimeout(500);
    					clientSocket.receive(receivePacket);
    					counter = 0;
    					pocitadloVzorka_good++;
       
    					receiveData = receivePacket.getData();
    					totalSize += (46+18); //jeden bajt je moja hlavicka
    					
    					delay = System.nanoTime();
    					
    					textArea.append("Prijate data z adresy: "+receivePacket.getAddress().toString().substring(1)+"\n");
    					vypisPrijatehoKodu(checkParita(receiveData[0], 4));
    					textArea.append("--------------------------------------------------------------\n");
    					
    					
         				delaySize += (System.nanoTime()-delay);
         		
    					b++;
    				}
    				fragments = 2;//ak by sa nespustila fragmentacia nech prejde vzorka iba raz
    				b = 1;//umoznuje poslat dalsie fragmenty novej vzorky
    				a++;
    			}
    			
    		clientSocket.close();
    		end = true;
    		
    		long endtime = System.nanoTime();;
    		long dataSize = (velkost*(Integer.parseInt(count)));
    		long firstSecondTime = ((endtime-startTime));
    		long totalUndelayedTime = firstSecondTime-delaySize;
    		firstSecondTime = totalUndelayedTime;
    		
    		//textArea.append("Cas straveny vypsimi " +firstSecondTime+" odcitane: "+totalUndelayedTime+"\n");
    		//textArea.append(totalSize+" "+dataSize+"\n");
    		textArea.append("Priemerny cas potrebny na prenesenie vzorky:\n" + 
    		(firstSecondTime/Integer.parseInt(count)) + " ns\n");
  
    		textArea.append("Priemerny cas potrebny na prenesenie 1 B vzorky:\n"+
    		(firstSecondTime/dataSize) + " ns\n");
    		
    		textArea.append("Priemerny cas potrebny,vo vseobecnosti,na prenesenie 1 B vzorky:\n"+
    	    		(firstSecondTime/totalSize) + " ns\n\n");
    		   	    		
    		
    		}catch(Exception e){
    			textArea.append("CHYBA POSIELANIA!!\nZlyhala vzorka c."+(a+1)+", fragment.c"+b+"\n");
    			pocitadloVzorka++;
    			//skok na znovu poslanie
    		}
    		
    		textArea.append("Pocet spravnych vzoriek: " +pocitadloVzorka_good+"\n");
    		textArea.append("Pocet chybnych vzoriek: "+pocitadloVzorka+"\n\n");
    		
    	}
    	
    	pocitadloVzorka_good = 0;
		pocitadloVzorka = 0;
    	
    	
    }
    
  
	public byte setFin(byte header){
    	header |= (1 << 0);//fin
    	return header;
    	
    }
    
    public byte packetHeader(int size){
		byte bit = 0;
		
		if(size % 2 == 0){
			bit |= (1 << 4);
			
		}
		else{
			bit &= ~(1 << 4);
		
		}
		return bit;
		
	}
    
    public int findNumberOfFragments(int size){
    	
    	if(size%MAX == 0){
    		return size/MAX;
    	}
    	else{
    		return (size/MAX +1);
    	}
    	
    }
    
    public int doFragmentation(int b, int velkost_vzorky){
			
		if((velkost_vzorky - (MAX*b)) >= 0){
			return MAX;
		}
		else
			return ((velkost_vzorky - (MAX*b))+MAX+1);
    	
    }
    
    public boolean checkParita(byte header, int index){
		return(header&(1<<index))!= 0;
		
	}
    
    public void vypisPrijatehoKodu(boolean parita){
		if(parita){
			textArea.append("Prijaty detekcny kod: 1\n");
		}
		else{
			textArea.append("Prijaty detekcny kod: 0\n");
		}
	}
	
	public void vypisVyratanehoKodu(boolean parita){
		if(parita){
			textArea.append("Vyratany detekcny kod: 1\n");
		}
		else{
			textArea.append("Vyratany detekcny kod: 0\n");
		}
	}
    
}