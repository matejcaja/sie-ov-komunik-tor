import java.net.*;
import java.util.Arrays;
import java.util.Random;

import javax.swing.JTextArea;

public class UDPServer extends Thread{
	JTextArea textArea = null;
	int random_number = 0;
	int jednotky;
	int pocitadlo = 0;
	public static int pocitadloACK = 0;
	public static int pocitadloVzorka = 0;
	public static int pocitadloVzorka_good = 0;
	
	
	public UDPServer (JTextArea textArea){
		this.textArea = textArea;
	}

	public void run() {
		try {
			textArea.append("Spustam server!\n");
			ServerRun();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void ServerRun() throws Exception{
			 int portGUI = GUI.getPort2();
			 
			 
			 DatagramSocket serverSocket = new DatagramSocket(portGUI);//3333
			 textArea.append("Cakam na data od klienta...\n\n");
			 byte[] receiveData = new byte[1471];
             byte[] sendData = new byte[18]; 
             
             
             Arrays.fill(receiveData, (byte)0);
             Arrays.fill(sendData, (byte)0);
             

             while(true)
                {  
            	 	DatagramPacket receivePacket = new DatagramPacket(new byte[1471], 1471);
                   	serverSocket.receive(receivePacket); 

                    receiveData = receivePacket.getData();
                    jednotky = 0;

                   for(int i = 1; i < receiveData.length; i++){
                	   if(receiveData[i] == 1){
                		   jednotky++;
                	   }
                   }
                   
                   byte serverParita;
                   serverParita = setParita(jednotky);  

                   textArea.append("Prijate data z adresy: "+receivePacket.getAddress().toString().substring(1)+"\n");
                   vypisPrijatehoKodu(checkParita(receiveData[0], 4,0));
                   vypisVyratanehoKodu(checkParita(serverParita, 4,0));
                   
                   
                   
                   textArea.append("\n");//oddelovac prijateho - odosielaneho
                   
                	   Random generator = new Random( System.currentTimeMillis() );
                	   random_number = generator.nextInt(100);

                	   InetAddress IPAddress = receivePacket.getAddress();
                	   int port = receivePacket.getPort(); 
                   
                	   sendData[0] = packetHeader(sendData.length);               	
                   	
                	   for(int j = 1; j < 18; j++){
                		   sendData[j] = 0;
                	   }
                   	
                	   DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port); // pripravim packet
                	   
                	   if((checkParita(serverParita, 4,1) == checkParita(receiveData[0], 4,0)))
                		   pocitadloVzorka_good++;
                	  
                	   if((checkParita(serverParita, 4,1) == checkParita(receiveData[0], 4,0))){//ak sa nezhoduju parity, neposle sa ack 
                		   
                		   
                		   
                		  
                		   
                   		if((random_number > GUI.getRandom())){//nahodne sa neposle ack
                   			serverSocket.send(sendPacket);     // odoslem ack packet ak je vsetko v poriadku
                   			
                   			pocitadloACK++;
                   			
                   			textArea.append("Posielam ACK c."+pocitadloACK+"\n");
                   			vypisVyratanehoKodu(checkParita(sendData[0], 4,0));
                   			textArea.append("--------------------------------------------------------------\n");
                   			
                   			
                   		   // if(checkFin(receiveData[0], 0)){
                   		    
                     		 // pocitadloVzorka = 0;
                     		  //pocitadloACK = 0;
                     		  //pocitadloVzorka_good = 0;
                     	  //}

                   		}
                   }else{
                	  
                	   pocitadloVzorka++; 
                   }	
                	   textArea.append("Pocet spravnych vzoriek: " + pocitadloVzorka_good + "\n"); 
                	   textArea.append("Pocet chybnych vzoriek: " + pocitadloVzorka + "\n\n");
                       
                
                }
            
       }
	
	public boolean checkFin(byte header, int index){
			return(header&(1<<index))!= 0;
	}
	
	public boolean checkParita(byte header, int index, int i){
		return(header&(1<<index))!= 0;
		
	}
	
	public byte setParita(int size){
		byte bit = 0;
		
		if(size % 2 == 0){
			bit |= (1 << 4);
		}
		else{
			bit &= ~(1 << 4);
		}
		return bit;
	}
	
	public byte packetHeader(int size){
		byte bit = 0;
		
		if(size % 2 == 0){
			bit |= (1 << 4);
		}
		else{
			bit &= ~(1 << 4);
		}

		return bit;
		
	}
	
	public void vypisPrijatehoKodu(boolean parita){
		if(parita){
			textArea.append("Prijaty detekcny kod: 1\n");
		}
		else{
			textArea.append("Prijaty detekcny kod: 0\n");
		}
	}
	
	public void vypisVyratanehoKodu(boolean parita){
		if(parita){
			textArea.append("Vyratany detekcny kod: 1\n");
		}
		else{
			textArea.append("Vyratany detekcny kod: 0\n");
		}
	}
	
}