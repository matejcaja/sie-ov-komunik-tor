// v.final with ports 25.4.2013

import java.awt.EventQueue;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.CardLayout;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;


public class GUI extends JFrame {

	 private static final long serialVersionUID = 1L;
	 private JPanel contentPane;
	 private JPanel card1 = new JPanel();
	 private JPanel card2 = new JPanel();
	 private JPanel card3 = new JPanel();
	 private CardLayout cardLayout = new CardLayout(0, 0);
	 private static JTextField textField;
	 final JTextArea textArea_2 = new JTextArea();
	 private static JTextField textField_1;
	 private static JTextField textField_2;
	 final JTextArea textArea = new JTextArea();
	 JButton btnSend = new JButton("send");
	 JButton btnBack = new JButton("back");
	 JButton btnKlient = new JButton("client");
	 JButton btnServer = new JButton("server");
	 private final JLabel lblChooseYourSide = new JLabel("Choose your side:");
	 private final JButton btnNewButton_1 = new JButton("clear");
	 static boolean created = false;
	 static public JTextField textField_3;
	 static public JTextField textField_4;
	 static public JTextField textField_5;
	 static public JTextField textField_6;
	 
	 UDPServer server = new UDPServer(textArea_2);
	 UDPClient client;
	 
	 
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {	
		
				
		EventQueue.invokeLater(new Runnable() {			
			
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}	


	/**
	 * Create the frame.
	 */
	public GUI() {		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 532, 448);
		contentPane = new JPanel();
		
		
		  contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		  setContentPane(contentPane);
		  contentPane.setLayout(null);
		  contentPane.setLayout(cardLayout);
		  contentPane.add(card1, "1");
		  card1.setLayout(null);
		  
		 
		  
		  btnKlient.setBounds(128, 180, 97, 25);
		  card1.add(btnKlient);
		  
		 
		  
		  btnServer.setBounds(305, 180, 97, 25);
		  card1.add(btnServer);
		  lblChooseYourSide.setBounds(197, 145, 181, 15);
		  
		  card1.add(lblChooseYourSide);
		  contentPane.add(card2, "2");
		  card2.setLayout(null);
		  
		  
		 
		  btnBack.setBounds(382, 363, 97, 25);
		  card2.add(btnBack);
		  
		  textField = new JTextField();
		  
		  
		  textField.setBounds(37, 20, 116, 22);
		  card2.add(textField);
		  textField.setColumns(10);
		  
		  JLabel lblZadajIp = new JLabel("Zadajte IP:");
		  lblZadajIp.setBounds(37, 0, 145, 16);
		  card2.add(lblZadajIp);
		  
		  JScrollPane scrollPane = new JScrollPane();
		  scrollPane.setBounds(37, 114, 442, 235);
		  card2.add(scrollPane);
		  
		  
		  scrollPane.setViewportView(textArea);
		  
		  
		  
		  btnSend.setBounds(37, 363, 97, 25);
		  card2.add(btnSend);
		  
		  textField_1 = new JTextField();
		  textField_1.setBounds(201, 20, 116, 22);
		  card2.add(textField_1);
		  textField_1.setColumns(10);
		  
		  textField_2 = new JTextField();
		  textField_2.setBounds(37, 80, 116, 22);
		  card2.add(textField_2);
		  textField_2.setColumns(10);
		  textArea.setEditable(false);
		  textArea_2.setEditable(false);
		  
		  JLabel lblPocetVzoriek = new JLabel("Zadajte pocet vzoriek:");
		  lblPocetVzoriek.setBounds(201, 0, 185, 16);
		  card2.add(lblPocetVzoriek);
		  
		  JLabel lblNewLabel = new JLabel("Zadajte velkost vzorky:");
		  lblNewLabel.setBounds(37, 54, 170, 16);
		  card2.add(lblNewLabel);
		  
		  JButton btnNewButton = new JButton("clear");
		 
		  btnNewButton.setBounds(382, 77, 97, 25);
		  card2.add(btnNewButton);
		  
		  JLabel lblParita = new JLabel("Parita:");
		  lblParita.setBounds(398, 1, 70, 15);
		  card2.add(lblParita);
		  
		  textField_4 = new JTextField("10");
		  textField_4.setBounds(398, 20, 63, 22);
		  card2.add(textField_4);
		  textField_4.setColumns(10);
		  
		  JScrollPane scrollPane_2 = new JScrollPane();
		  scrollPane_2.setBounds(157, 367, 191, 25);
		  card2.add(scrollPane_2);
		  
		  final JTextArea textArea_1 = new JTextArea();
		  scrollPane_2.setViewportView(textArea_1);
		  
		  JLabel lblPort = new JLabel("Port:");
		  lblPort.setBounds(234, 54, 70, 15);
		  card2.add(lblPort);
		  
		  textField_5 = new JTextField();
		  textField_5.setBounds(234, 81, 114, 19);
		  card2.add(textField_5);
		  textField_5.setColumns(10);
		  contentPane.add(card3, "3");
		  card3.setLayout(null);
		  
		  JButton btnBack_1 = new JButton("back");
		 
		  btnBack_1.setBounds(367, 355, 97, 25);
		  card3.add(btnBack_1);
		  
		  JScrollPane scrollPane_1 = new JScrollPane();
		  scrollPane_1.setBounds(56, 73, 408, 246);
		  card3.add(scrollPane_1);
		  scrollPane_1.setViewportView(textArea_2);
		  
		  final JButton btnListen = new JButton("start");
		  
		  btnListen.setBounds(57, 355, 97, 25);
		  card3.add(btnListen);
		  
		  btnNewButton_1.setBounds(367, 22, 97, 25);
		  
		  card3.add(btnNewButton_1);
		  
		  textField_3 = new JTextField("10");
		  textField_3.setBounds(112, 41, 42, 19);
		  card3.add(textField_3);
		  textField_3.setColumns(10);
		  
		  JLabel lblAck = new JLabel("ACK:");
		  lblAck.setBounds(56, 41, 70, 15);
		  card3.add(lblAck);
		  
		  JLabel lblPort_1 = new JLabel("Port:");
		  lblPort_1.setBounds(56, 12, 70, 15);
		  card3.add(lblPort_1);
		  
		  textField_6 = new JTextField();
		  textField_6.setBounds(112, 12, 114, 19);
		  card3.add(textField_6);
		  textField_6.setColumns(10);
		  
		  
		  btnNewButton_1.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent e) {
			  		textArea_2.setText("");
			  		UDPServer.pocitadloACK = 0;
			  		UDPServer.pocitadloVzorka = 0;
			  		UDPServer.pocitadloVzorka_good = 0;
			  	}
			  });
		  
		  btnNewButton.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent arg0) {
			  		textArea.setText("");
			  	}
			  });
		  
		  btnKlient.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent e) {
			  		cardLayout.show(contentPane, "2");
			  	}
			  });
		  
		  btnServer.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent e) {
			  		cardLayout.show(contentPane, "3");
			  	}
			  });
		  
		  btnBack.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent e) {
			  		cardLayout.show(contentPane, "1");
			  	}
			  });
		  
		  btnBack_1.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent e) {
			  		cardLayout.show(contentPane, "1");
			  		btnListen.setEnabled(true);
			  		
			  	}
			  });
		  
		  btnListen.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent e) {
			  		try {
			  			textFieldCheckServer();
			  			server.start();	
			  			btnListen.setEnabled(false);
					} 
			  		catch (Exception e1) {
						e1.printStackTrace();
					}
			  	}
			  });
		  
		
		  
		  btnSend.addActionListener(new ActionListener() {
			  	public void actionPerformed(ActionEvent e) {
			  		try {
			  			textFieldCheck();
			  			if(created == false){
			  				client = new UDPClient(textArea, contentPane, textArea_1);
			  				created = true;
			  					  			
			  			}else{
			  				client.interrupt();
			  				client = new UDPClient(textArea, contentPane,textArea_1);
			  			}
			  			client.start();

					} catch (Exception e1) {
						
						e1.printStackTrace();

					}
			  	}
			  });
	}
	
	public void textFieldCheck(){
		if(textField.getText().equals(""))
			textField.setText("127.0.0.1");
		
		if(textField_1.getText().equals(""))
			textField_1.setText("1");
		
		if(textField_2.getText().equals(""))
			textField_2.setText("200");
		
		if(textField_5.getText().equals(""))
			textField_5.setText("3333");

	}
	
	public void textFieldCheckServer(){

		if(textField_6.getText().equals(""))
			textField_6.setText("3333");

	}
	
	public static String getIP(){
		return textField.getText();
	}
	
	public static String getCount(){
		return textField_1.getText();
	}
	
	public static String getVelkost(){
		return textField_2.getText();
	}
	
	public static int getRandom(){
		return Integer.parseInt(textField_3.getText());
	}
	
	public static int getDamage(){
		return Integer.parseInt(textField_4.getText());
	}
	
	public static int getPort(){
		int port = Integer.parseInt(textField_5.getText());
				
		if(port > 1024)
			return Integer.parseInt(textField_5.getText());
		else{ 
			textField_5.setText("3333");
			return 3333;
		}
	}
	
	public static int getPort2(){
		int port = Integer.parseInt(textField_6.getText());
		
		if(port > 1024)
			return Integer.parseInt(textField_6.getText());
		else{ 
			textField_6.setText("3333");
			return 3333;
		}
	}
}
